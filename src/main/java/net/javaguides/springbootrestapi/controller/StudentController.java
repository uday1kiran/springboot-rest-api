package net.javaguides.springbootrestapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import net.javaguides.springbootrestapi.bean.Student;

import java.util.ArrayList;

@RestController
public class StudentController {
    //http://localhost:8080/student
    @GetMapping("student")
    public Student getStudent(){
        Student student = new Student(
                 1,
         "Ramesh",
         "Suresh"
        );
        return student;
    }
    @GetMapping("students")
    public List<Student> getStudents(){
        List<Student> students = new ArrayList<>();
        students.add(new Student(1,"Uday","Kiran"));
        students.add(new Student(1,"mani","prakash"));
        return students;
    }
}
